// @source https://stepik.org/lesson/228838/step/5?unit=201372
// Игнорирование возвращаемых значений
// Go позволяет проигнорировать все или определенные возвращаемые функцией значения,
//  если мы не будем использовать их в дальнейшем. Для этого нам необходимо не присваивать им имена вообще либо заменить имя символом _.
// Рассмотрим это на примере ниже.
// Здесь мы создали функцию-заглушку, возвращающую 2 значения: число и ошибка.
// В первом примере мы проигнорировали оба возвращаемых значения,
// выполнив полезную работу функции (так мы часто делаем, когда используем функцию fmt.Print - игнорируя возвращаемые ею значения).
// Во втором примере мы проигнорировали сообщение об ошибке (не делайте так),
// а в третьем - только проверили, возвратила ли функция ошибку, проигнорировав возвращаемое число.

package main

import "fmt"

func fn() (int, error) {
	// Какая-то полезная работа
	// ...
	return 0, nil
}

func ExampleIgnor() {
	fn()

	i, _ := fn()
	fmt.Println(i)

	_, err := fn()
	if err == nil {
		fmt.Println("Ошибок нет")
	}
}

func main() {
	ExampleIgnor() // Output:  0 Ошибок нет
}
